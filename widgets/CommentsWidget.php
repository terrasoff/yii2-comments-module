<?php

namespace terrasoff\yii2\comments\widgets;

use terrasoff\yii2\comments\models\Comment;
use terrasoff\yii2\comments\Module as CommentModule;
use yii\base\Widget;
use yii\db\ActiveRecord;

/**
 * Comment behaviot
 * @author terrasoff
 */
class CommentsWidget extends Widget
{
    /**
     * Entity for comments
     * @var ActiveRecord $model
     */
    public $model;

    /**
     * Comments type:
     *      CommentModule::TYPE_LINEAR
     *      CommentModule::TYPE_NESTED
     * @var int $type
     */
    public $type = CommentModule::TYPE_LINEAR;

    /**
     * Comments entity's class name
     * @var string
     */
    public $idClass = null;

    public $view = 'form';

    public function buildTree(array $models) {
        $tree = array();
        foreach ($models as $model) {
            $parent = (int)$model->idParent;
            if (!isset($tree[$parent])) {
                $tree[$parent] = array();
            }

            $tree[$parent][] = $model;
        }
        return $tree;
    }

    public function renderTree($tree, $type, $index = 0) {
        $html = '<ul>';
        if (!empty($tree))
            foreach($tree[$index] as $node) {
                $html.= $this->render('comment',[
                    'model'=>$node,
                    'type'=>$type,
                ]);
                if (isset($tree[$node->idComment]) && $this->type === CommentModule::TYPE_NESTED)
                    $html.= $this->renderTree($tree, $type, $node->idComment);
            }
        return $html.'</ul>';
    }

    public function run()
    {
        $comments = $this->renderTree($this->buildTree($this->model->comments), $this->type);

        return $this->view
            // render comment form
            ? $this->render($this->view,[
                'comments' => $comments,
                'className' => $this->model->getBehavior('comments')->getClassName(),
                'idClass' => $this->model->primaryKey,
            ])
            // raw html list
            : $comments;
    }
}