# Description
Yii2 comment module. You can use this module to add comments to any entity (class) of your projects.
For example, it could be added to Post or Photo class.

#How to use this behavior
## Add module

	// yii config file
	'modules' => [
		'comments' => [
			'class' => 'terrasoff\yii2\comments\Module',
		],
	],



## Add model behavior

	function behaviors()
	{
		return [
			'comments'=> [
				'class' => \terrasoff\yii2\comments\models\CommentBehavior::className(),
				// this model need captcha to save comments
				'captchaModels' => ['terrasoff\yii2\blog\models\Post'],
			],
		];
	}

## Add comment widget in model view

	<?php echo CommentsWidget::widget([
		'model' => $model,
		'type' => \terrasoff\yii2\comments\Module::TYPE_NESTED,
	]);?>