$(document).ready(function() {

    var $modal = $('#CommentModal');
    var $comments = $('#comments');
    var $form = $modal.find('form');
    var $formComment = $modal.find('.comment');
    var $formParentId = $form.find('[name="Comment[idParent]"]');
    var $currentComment = null;

    $('.comments').on('click', '.comment-reply', function(e) {
        $currentComment = $(this).parent('.comment');
        var idComment = $currentComment.data('id');
        $formParentId.val(idComment);
        $formComment.hide(idComment > 0);
        if (idComment > 0)
            $formComment.replaceWith($currentComment.find('.text').clone());

        $modal.modal();
    });

    function sendForm($form, url) {
        return $.ajax({
            type: 'POST',
            dataType: "json",
            url: url,//
            data: $form.serialize()
        });
    }

    $form.on('submit', function(e) {
        e.preventDefault();
        sendForm($form, "/comments/comment/save")
            .done(function(re) {
                // errors
                if (re.errors != undefined) {
                    console.dir(re.errors);
                    // no errors
                } else {
                    $comments.html(re.result);
                    $modal.modal('hide');
                }
            })
            .fail(function(jqXHR) {
                $modal.modal('hide');
            });
    });

    $('.comment-update').on('click', function(e) {
        sendForm($form, "/comments/comment/get")
            .done(function(re) {
                // errors
                console.dir($comments);
                console.dir(re);
                if (re.errors != undefined) {
                    console.dir(re.errors);
                    // no errors
                } else {
                    $comments.html(re.result);
                    $modal.modal('hide');
                }
            })
            .fail(function(jqXHR) {
                $modal.modal('hide');
            });
    });

});