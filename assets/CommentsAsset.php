<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace terrasoff\yii2\comments\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CommentsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $js = [
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function init() {
        $this->sourcePath = __DIR__.DIRECTORY_SEPARATOR.'bundles';
        parent::init();
    }
}
