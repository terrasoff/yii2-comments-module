<?php

namespace terrasoff\yii2\comments\controllers;

use Yii;
use terrasoff\yii2\blog\models\Post;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\Controller;
use terrasoff\yii2\comments\models\Comment;
use terrasoff\yii2\comments\Module as CommentModule;

class CommentController extends Controller
{
    public function init() {
        parent::init();
        $this->layout = $this->module->layout;
    }

    public function actions()
    {
        return [
            'save' => [
                'class' => 'terrasoff\yii2\actions\Save',
                'model' => Comment::className(),
                'hasCaptcha' => function($className) {
                    return Yii::$app->getModule('comments')->hasCaptcha($className);
                },
                'onSuccess' => function($model) {
                    /** @var Action $this */
                    return $this->_update($model);
                }
            ],
        ];
    }

    public function actionGet() {
        $re = [];
        $comment = new Comment();
        $comment->load(Yii::$app->request->post());
        $re['result' ] = $this->_update($comment);
        return Json::encode($re);
    }

    private function _update($model) {
        /** @var Comment $model */
        return Yii::$app->getModule('comments')->getComments(
            $model->className,
            $model->idClass,
            CommentModule::TYPE_NESTED
    );
}

}