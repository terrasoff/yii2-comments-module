<?php

namespace terrasoff\yii2\comments\models;

use Yii;
use yii\behaviors;
use yii\db;

/**
 * This is the model class for table "Comment".
 *
 * @property int $idComment
 * @property int $idUser
 * @property string $className
 * @property int $idClass
 * @property string $username
 * @property string $text
 * @property int $rating
 * @property int $created
 * @property int $updated
 * @property int $state
 */
class Comment extends \yii\db\ActiveRecord
{

    const STATE_BLOCK = 'block'; // flag behavior: comment is blocked

    public $captcha = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idClass', 'className', 'username', 'text', 'idParent'], 'required'],
            [['className'], 'string'],
            [['idClass', 'idParent'], 'integer'],
            [['text'], 'string'],
            [['username'], 'string', 'max' => 45],
            [['captcha'], 'captcha', 'on'=>'captcha'],
        ];
    }

    function behaviors()
    {
        return [
            'timestamp'=> [
                'class'=> behaviors\AttributeBehavior::className(),
                'attributes'=> [
                    db\ActiveRecord::EVENT_BEFORE_INSERT => ['updated', 'created'],
                    db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value'=> function() {return new db\Expression('NOW()');},
            ],
            'flags'=> [
                'class'=> \terrasoff\yii2\FlagBehavior::className(),
                'fieldName'=> 'state',
                'flags'=> [
                    self::STATE_BLOCK => false,
                ],
            ]
        ];
    }

}