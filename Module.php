<?php

/*
 * terrasoff comments module
 */

namespace terrasoff\yii2\comments;

use Yii;
use terrasoff\yii2\comments\widgets\CommentsWidget;
use yii\base\Exception;
use yii\base\Module as BaseModule;
use yii\caching\DbDependency;

class Module extends BaseModule
{
    const TYPE_LINEAR = 1;
    const TYPE_NESTED = 2;

    /**
     * list of model that need captcha to post comment
     * @param $model
     * @return bool
     */
    public $captchaModels = [];

    public $captchaAction = '/site/captcha';

    public function init()
    {
        parent::init();
        // add module's dictionaries
        \Yii::$app->i18n->translations['comment*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => __DIR__.'/messages',
        ];
    }

    /**
     * check if model's class need captcha to post comment
     * @param $className
     * @return bool
     */
    public function hasCaptcha($className) {
        return in_array($className, $this->captchaModels);
    }

    /**
     * Ajax comments updating
     * @param string $className
     * @param int $idClass
     * @param string $type
     * @return string html
     * @throws \yii\base\Exception
     */
    public function getComments($className, $idClass, $type)
    {
        $model = $className::find()
            ->where(['idPost'=>$idClass])
            ->one();

        if (!$model)
            throw new Exception('Объект не найден', 404);

        // only comments
        return CommentsWidget::widget([
            'view' => false,
            'model'=> $model,
            'type' => $type
        ]);
    }
}